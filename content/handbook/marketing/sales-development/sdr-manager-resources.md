---

title: Sales Development Manager Resources
description: This depracated page walks you through all the tools we use in the Sales Dev org.

---

## You can now find our new [Manager Resources page here](/handbook/marketing/sales-development/sales-development-tools/#sales-dev-manager-resources)
